Source: stklos
Section: lisp
Priority: optional
Maintainer: Debian Scheme Dream Team <debian-scheme@lists.debian.org>
Uploaders: Göran Weinholt <weinholt@debian.org>
Build-Depends: debhelper-compat (= 12),
 libffi-dev,
 libpcre3-dev,
 libgmp-dev,
 libgc-dev
Standards-Version: 4.5.0
Homepage: https://stklos.net/
Vcs-Browser: https://salsa.debian.org/scheme-team/stklos
Vcs-Git: https://salsa.debian.org/scheme-team/stklos.git

Package: stklos
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: stklos-doc
Description: object-oriented Scheme interpreter with GTK+ integration
 STklos is a free Scheme system compliant with the languages features
 defined in R7RS. The aim of this implementation is to be fast as well
 as light. The implementation is based on an ad-hoc Virtual Machine.
 STklos can also be compiled as a library and embedded in an
 application.
 .
 STklos is the successor of STk, a Scheme interpreter tightly
 connected to the Tk toolkit. Whereas STk used the Tk toolkit, STklos
 uses the GTK+ graphical toolkit.

Package: stklos-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Build-Profiles: <!nodoc>
Description: object-oriented Scheme interpreter (documentation)
 STklos is a free Scheme system compliant with the languages features
 defined in R7RS.
 .
 This package provides the documentation.
